ping
=====================================
Метод проверки соединения и получения инвормации о текушем авторизованном
пользователе

## Параметры запроса

нет

## Параметры ответа

- user
    + id (int) - идентификатор пользователя
    + login (string) - имя пользователя
    + avatar (string) - url изображения-аватара пользователя


## Пример запроса


## Пример ответа

```
{
    "errors_count":0,
    "errors":[],
    "result":{
        "user":{
            "id":1,
            "login":"chesser",
            "avatar":"http://newwaysys.com/wp-content/uploads/sk/thumb-sketch-of-a-knight-chess-piece-vectors.jpg"
        }
    },
    "session_id":"17a938271d82987e02847503957"
}
```

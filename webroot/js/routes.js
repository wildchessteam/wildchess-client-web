/**
    Определение привязки контроллеров 
    (какой адрес каким контролером обрабатывается)
    и директивы AngularJs
*/

var main = angular.module("main", ['ngRoute','ngCookies']);

// Определение контроллеров
main.config(function($routeProvider){

    // Конроллер главной страницы
    $routeProvider.when("/",{
        controller: "index",
        templateUrl:"views/controllers/index.html"
    });

    // Конроллер top рейтинга
    $routeProvider.when("/rating/top/",{
        controller: "topRating",
        templateUrl:"views/controllers/topRating.html"
    });

    // Конроллер top рейтинга
    $routeProvider.when("/rating/:userId/",{
        controller: "userRating",
        templateUrl:"views/controllers/userRating.html"
    });

    
});


/**
Директива элемента <input type="file">, позволяющая получить содержимое
загруженного файла на клиенте

При загрузке файла вызывается функция showContent и содержимое файла передаётся
в переменную $content

<input type="file" on-read-file="showContent($content)" /> 

*/
main.directive('onReadFile', function ($parse) {
	return {
		restrict: 'A',
        scope: {
            onReadFile : "&"
        },
		link: function(scope, element, attrs) {
			element.on('change', function(e) {
				var reader = new FileReader();
                var files = (e.srcElement || e.target).files;
				reader.onload = function(e) {
					scope.$apply(function() {
                       scope.onReadFile({$content:files});
					});
				};
                reader.readAsDataURL(files[0]);
			});
		}
	};
});


// ckeditor, добавить к нужному textarea class="ck-editor"
main.directive('ckEditor', [function () {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, elm, attr, model) {
            var isReady = false;
            var data = [];
            var ck = CKEDITOR.replace(elm[0]);
            
            function setData() {
                if (!data.length)return;
                var d = data.splice(0, 1);
                ck.setData(d[0] || '<span></span>', function () {
                    setData();
                    isReady = true;
                });
            }

            ck.on('instanceReady', function (e) {
                if (model)setData();
            });
            
            elm.bind('$destroy', function () {
                ck.destroy(false);
            });

            if (model) {
                ck.on('change', function () {
                    scope.$apply(function () {
                        var data = ck.getData();
                        if (data == '<span></span>') {
                            data = null;
                        }
                        model.$setViewValue(data);
                    });
                });

                model.$render = function (value) {
                    if (model.$viewValue === undefined) {
                        model.$setViewValue(null);
                        model.$viewValue = null;
                    }

                    data.push(model.$viewValue);

                    if (isReady) {
                        isReady = false;
                        setData();
                    }
                };
            }
            
        }
    };
}]);



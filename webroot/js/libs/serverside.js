'use strict';

class ServerSide{
    constructor(objHttp,objCookie){
        // Если работаем локально - ищем серверсайд на порту 8101
        if(
            1
            || document.location.host=='127.0.0.1:8887'
            || document.location.host=='localhost:8100'
            || document.location.host=='127.0.0.1:8100'
        )
            this.apiUrl = 'http://localhost:8101/api/';
        else
            this.apiUrl = '/api/';

        this.http = objHttp;
        this.httpOptions = {
            //headers:{'Content-Type':'application/x-www-form-urlencoded'},
            headers:{'Content-Type':undefined},
            async:   false
        }
        this.error = {};
        this.userId = 0;
        this.allItems = 10000;
    }


    array_unuque_merge(array1,array2){
        let result = array1;
        let flag = false;
        for(let i in array2){
            flag = false;
            for(let j in array1){
                if(JSON.stringify(array2[i])==JSON.stringify(array1[j])){
                    flag = true;
                    break;
                }
            }
            if(!flag)result.push(array2[i]);
        }
        return result;
    }

    /**
        Получение даты в локальном формате
        @param strDate - дата в виде строки. Может быть
            * Y-m-d H:i:s
            * m/d/Y H:i:s
            * d.m.Y H:i:s
            * timestamp
    */
    localDatetime(strDate){
        let reRus = /^(\d+)\.(\d+)\.(\d+)\s+(\d+):(\d+):(\d+)$/;    
        let reEng = /^(\d+)\/(\d+)\/(\d+)\s+(\d+):(\d+):(\d+)$/;    
        let reIso = /^(\d+)\-(\d+)\-(\d+)\s+(\d+):(\d+):(\d+)$/;
        let reTimestamp = /^(\d+)$/;
        let format = '';
        let timestamp = 0;

        let m = new Array();
        // Если дата d.m.Y H:i:s
        try{m = strDate.match(reRus);}catch(e){}
        if(m){
            timestamp = (
                new Date(m[3],m[2]-1,m[1],m[4],m[5],m[6])
            ).getTime();
            format = 'rus';
        }
        
        // Если дата m/d/Y H:i:s
        try{m = strDate.match(reEng);}catch(e){}
        if(m){
            timestamp = (new Date(m[3],m[1]-1,m[2],m[4],m[5],m[6])).getTime();
            format = 'eng';
        }

        // Если дата Y-m-d H:i:s
        try{m = strDate.match(reIso);}catch(e){}
        if(m){
            timestamp = (new Date(m[1],m[2]-1,m[3],m[4],m[5],m[6])).getTime();
            format = 'iso';
        }

        // Если дата timestamp
        try{m = strDate.match(reTimestamp);}catch(e){}
        if(m){
            timestamp = parseInt(strDate)*1000;
            format = 'timestamp';
        }
        if(!timestamp)return '';

        // Переводим timestamp в локальное время браузера
        if(format=='timestamp'){
            timestamp = parseInt(timestamp/1000)-parseInt((new Date()).getTimezoneOffset())*60;
        }
        else{
            timestamp = parseInt(timestamp/1000)-parseInt((new Date()).getTimezoneOffset())*60;
        }

        let date = new Date(timestamp*1000);

        let minutes = '0'+String(date.getMinutes());
        let hours = '0'+String(date.getHours());
        let seconds = '0'+String(date.getSeconds());
        let year = String(date.getYear()+1900);
        let mon = '0'+String(date.getMonth()+1);
        let day = '0'+String(date.getDate());
        strDate = '';

        format='rus';// фиксируем формат вывода только русский
        if(format=='rus')
            strDate = day.substr(-2)+'.'+mon.substr(-2)+'.'+year.substr(-4)+
            ' '+hours.substr(-2)+':'+minutes.substr(-2)+':'+seconds.substr(-2);
        if (format=='eng')
            strDate = mon.substr(-2)+'.'+day.substr(-2)+'.'+year.substr(-4)+
            ' '+hours.substr(-2)+':'+minutes.substr(-2)+':'+seconds.substr(-2);
        if(format=='iso')
            strDate = year.substr(-4)+'-'+mon.substr(-2)+'-'+day.substr(-4)+
            ' '+hours.substr(-2)+':'+minutes.substr(-2)+':'+seconds.substr(-2);
        if(format=='timestamp')
            strDate = day.substr(-2)+'.'+mon.substr(-2)+'.'+year.substr(-4)+
            ' '+hours.substr(-2)+':'+minutes.substr(-2)+':'+seconds.substr(-2);

        return strDate;        
    }

    /**
        Перевод даты в виде строки в timestamp
    */
    date2timestamp(strDate,bEndDate){
        bEndDate = bEndDate || false;
        let reRus = /^(\d+)\.(\d+)\.(\d+)$/;
        let reEng = /^(\d+)\/(\d+)\/(\d+)$/;
        let matches = new Array;
        let timestamp = 0;


        if((matches = strDate.match(reRus)).length){
            timestamp = (new Date(
                matches[3],matches[2]-1,matches[1]
            )).getTime();
        }
        else if((matches = strDate.match(reEng)).length){
            timestamp = (new Date(
                matches[3],matches[1]-1,matches[2]
            )).getTime();
        }
        else
            timestamp = Date.parse(strDate);

        timestamp = parseInt(timestamp/1000);//-(new Date()).getTimezoneOffset()*60;
        if(bEndDate)timestamp+=24*60*60 -1;
        return timestamp;
    }

    /**
        Получение base64 изображения

        @param img - dom-объект изображения <mg>
    */
    src2date(img){
        let name=img.getAttribute('src').split(/[\/\\]/).reverse()[0];
        let ext = name.split(".").reverse()[0];
        let mime = '';
        switch(ext){
            case 'jpg':
                mime = 'image/jpeg';
            break;
            case 'jpeg':
                mime = 'image/jpeg';
            break;
            case 'png':
                mime = 'image/png';
            break;
            case 'gif':
                mime = 'image/gif';
            break;
        }
        
        
        let canvas = document.createElement("canvas");
        canvas.width = img.naturalWidth;
        canvas.height = img.naturalHeight;
        
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        return {
            "name":name,
            "data":canvas.toDataURL(mime)
        }
    }
 
    // Вызов метода serverside
    send(methodName, params){return this.__request(methodName,params);}

    // Вызов сообщения об успехе
    riseSuccess(text, hideTime){
        $('.success').html(text);
        $('.success').show('fast');
        setTimeout(function(){
            $('.success').hide('fast');
        },hideTime);
        return false;
    }

    // Вызов подтвеждения дейтвия
    riseConfirm(text){return confirm(text);}

    // ВЫзов сообщения об ошибке
    riseError(method, errors){
        $('#root-error').html(method+errors.join(';'));
        $('#root-error').slideDown();
        setTimeout(function(){
            $('#root-error').slideUp(400,function(){$('#root-error').html('');});
        },3000);
        return false;
    }

    // Обработка вызова метода serverside
    __request(method, parameters){
        let self = this;
        return new Promise(function(succeccCallback,failedCallback){
            let formData = new FormData();
            formData.append('method',method);
            formData.append('params',JSON.stringify(parameters));

            // Пробуем обратиться к serverside
            self.http.post(self.apiUrl,formData,self.httpOptions)
            .then(
                // Если серверсайд ответил 200-й, парсим и обрабатываем ответ
                function(response){
                    if(response.data.errors_count==0){
                        if(
                            typeof(response.data.result.total)!='undefined'
                            && typeof(response.data.result.page)!='undefined'
                            && typeof(response.data.result.on_page)!='undefined'
                        )response.data.result.pagination = self.__pagination(
                            response.data.result.total,
                            response.data.result.page,
                            response.data.result.on_page
                        );
                        succeccCallback(response.data.result);
                    }
                    else{
   
                    }
                },
                // Если серверсайтответил не 200-й пытаемся загрузить
                //mock-данные
                function(response){
                    self.http.get('/mock/'+method+'.json' ,self.httpOptions)
                    .then(
                        function(response){
                            if(response.data.errors_count==0){
                                if(
                                    typeof(response.data.result.total)!='undefined'
                                    && typeof(response.data.result.page)!='undefined'
                                    && typeof(response.data.result.on_page)!='undefined'
                                )response.data.result.pagination = self.__pagination(
                                    response.data.result.total,
                                    response.data.result.page,
                                    response.data.result.on_page
                                );
                                succeccCallback(response.data.result);
                            }
                            else{
                                self.riseError(method, response.data.errors);
                                failedCallback(response.data.errors);
                            }
                        },
                        function(response){
                        }
                    );
                },
            )
        });
    }

    __responseProcess(response){
    }
    
    __pagination(total,page,onpage){
        let blockSize = 7;
        
        let totalPages = Math.floor((total-1)/onpage)+1;
        
        let totalBlocks = Math.floor((totalPages-1)/blockSize)+1;
        let startBlockPage = Math.floor((page-1)/blockSize)*blockSize+1;
        let endBlockPage = startBlockPage+blockSize-1;
        if(endBlockPage>totalPages)endBlockPage = totalPages;
        
        let pages = [];
        if(startBlockPage>blockSize){
            pages.push({page:1,title:1,active: false});
            pages.push({page:startBlockPage-1,title:'...',active: false});
        }
        for(let i=startBlockPage;i<=endBlockPage;i++)
            pages.push({page:i,title:i,active: page==i});
            
        if(totalPages>(endBlockPage+1))
            pages.push({page:endBlockPage+1,title:'...',active: false});
        
        if(totalPages>endBlockPage)
            pages.push({page:totalPages,title:totalPages,active: false});
            
        if(pages.length<2)return [];
        
        return pages;
    }
    
}

// Контроллер для обработки главной страницы
//Ls
main.controller('index',function($scope,$http,$location,$cookies){
    // Период обновления списка игроков в миллисекундах
    let updateInterval = 10000;
    // Создаём объект для работы с сервером
    var WCApi = new ServerSide($http,$cookies);

    // Функция обновления списка игроков
    $scope.gamersUpdate = function(data){$scope.gamers = data;}

    // Функция обновления списка игр
    $scope.gamesUpdate = function(data){$scope.games = data;}

    // Функция обновления чата
    $scope.chatUpdate = function(data){

        // Запоминам предыдущие сообщения
        var previousMessages = $scope.chat; 

        for(let i in data.messages){
            data.messages[i]["date"] = WCApi.localDatetime(
                data.messages[i].timestamp
            );
        }
        // Сохраняем полученные сообщения в scope
        $scope.chat = data;

        // Если первая загрузка (предыдущих сообщений нет), то сразу проматываем
        // в конец без анимации. Иначе проматываем медленно
        if(!previousMessages)
            $scope.chatScrollToEnd(1);
        else 
            $scope.chatScrollToEnd(500);        
    }
    
    // Функция, обновляющая всю информацию на главной странице
    $scope.allUpdate = function(){
        WCApi.send('hallGamers',{})
            .then(function(data){ $scope.gamersUpdate(data) });
        WCApi.send('hallGames',{})
            .then(function(data){ $scope.gamesUpdate(data) });
        WCApi.send('hallChat',{})
            .then(function(data){ $scope.chatUpdate(data) });
    }

    // Показывает окно создания новой партии
    $scope.showNewGame = function(){
		// Данные новой партии по умолчанию
		$scope.$parent.newGameInfo = {
			"name":"",
			"types":{
				"nobody":{"id":0,"name":"Не выбрано"},
				"human":{"id":1,"name":"Человек"},
				"cpu":{"id":2,"name":"Компьютер"}
			},
			"sides":{
				"white":{"name":"Белые","type":0},
				"blue":{"name":"Синие","type":0},
				"black":{"name":"Черные","type":0},
				"red":{"name":"Красные","type":0}
			},
			"require_password":false,
			"password":"",
			"viewers":true,
			"time_control":false,
			"time_limit": 2
		};
		
		// Загрузить модальное окно новой партии
        $scope.loadModal('newGame');
        // Показать окно
        $scope.showModal();
    }
    
    // Отправляет сообщение в чат
    $scope.sendMessage = function(){
        if($scope.chatMessage == '')return false;

        // Отправлем сообщение на сервер
        WCApi.send('sendMessage',{text:$scope.chatMessage,gameId:null})
            .then(function(data){});

        // Немедленно добавляем соощение в окно, не дожидаясьответа
        $scope.chat.messages.push({
            date:WCApi.localDatetime(Math.round(new Date().getTime()/1000)),
            user:{
                id:$scope.$parent.currentUser.id,
                login:$scope.$parent.currentUser.login,
                avatar:$scope.$parent.currentUser.avatar,
            },
            text:$scope.chatMessage
        });

        // Очищаем поле ввода 
        $scope.chatMessage = '';
        // Перематываем сообщения в конец
        $scope.chatScrollToEnd(500);
    }

    // Промотать чат до низу
    // speed - скорость промотки (миллисекунд)
    $scope.chatScrollToEnd = function(speed){
        speed = speed || 0;
        let target = $('div.hall-chat div.messages'); 
        let wrap =  $('div.hall-chat div.messages .messages-wrap'); 
        target.animate({scrollTop: wrap.height()}, speed);        
    }


    // Обновляем информацию на странице после первой загрузки
    $scope.allUpdate();

    // Устанавливаем периодическое выполнение функции обновления информации
    setInterval(function(){$scope.allUpdate();},updateInterval);



});


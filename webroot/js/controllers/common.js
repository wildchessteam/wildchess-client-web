/**
    Контроллер, выполняющийся при формировании каждой страницы
*/
main.controller('common',function($scope,$http,$location,$cookies){

    // Интервал отправки пинга (миллисекунд)
    let pingInterval = 10000;
    
    // Фора модального окна по умолчанию
    $scope.modalName = 'none';

    
    // Объектдля запросов к серверу
    let WCApi = new ServerSide($http,$cookies);
    
    // Выставляем адрус сервера по умолчанию
    $scope.serverside = $cookies.get('serverside') || WCApi.apiUrl;
    
    // Первый ping-запрос
    WCApi.send('ping',{}).then(function(data){$scope.profileUpdate(data)});
    
    // Периодический пинг сервера (в ответе информация о пользователе)
    setInterval(function(){
        WCApi.send('ping',{})
            .then(function(data){ $scope.profileUpdate(data) });
    },pingInterval);

    // Обновление профиля пользователя по ответу ping-запроса
    $scope.profileUpdate = function(data){$scope.currentUser = data.user;}
    
    // Показывает окно входа
    $scope.showLoginModal = function(){
        $scope.loadModal('login');
        $scope.showModal();
    }

    // Показывает окно регистрации
    $scope.showRegisterModal = function(){
        if(typeof($scope.registerForm)=='undefined')
            $scope.registerForm = {email:""};
        $scope.loadModal('register');
        $scope.showModal();
	
    }
	

    // Показывает окно подтверждения регистрации
    $scope.showCheckRegisterModal = function(){
        if(typeof($scope.checkRegisterForm)=='undefined')
            $scope.checkRegisterForm = {code:""};
        $scope.loadModal('check_register');
        $scope.showModal();
    }

    // Показывает окно восстановления пароля
    $scope.showRemindPasswordModal = function(){
        if(typeof($scope.remindPasswordForm)=='undefined')
            $scope.remindPasswordForm = {email:""};
        $scope.loadModal('remind_password');
        $scope.showModal();
    }

    // Показывает окно выхода
    $scope.showLogoutModal = function(){
        $scope.loadModal('logout');
        $scope.showModal();
    }
    
    // Показывает окно профиля
    $scope.showProfileModal = function(userId){
        $scope.loadModal('profile');
        $scope.showModal();
        $scope.modalProfile = {};
        WCApi.send('profile',{id:userId})
            .then(function(data){
                data.id=userId;
                $scope.modalProfile = data;
            });
    }
    
    // Загружаем модальное окно
    $scope.loadModal = function(name){$scope.modalName =  name;}
    
    // Показывает текущее модальное окно
    $scope.showModal = function(filename){
        // ПУстое окно не показываем
        if($scope.modalName==='none')return false;
        // А не пустое - показываем
        $('#Modal').modal('show');
        // При событии "закрытие окна" обнуляем адрес шаблона окна
        $('#Modal').on('hidden.bs.modal', function (e) {
            $scope.modalName =  'none';
            $scope.$apply();
        })        
    }
    
	// Добавление новой игры
    $scope.addNewGame = function(){
		// Проверка наличия имени партии
		if($scope.newGameInfo.name=="")
			return WCApi.riseError('',['Необходимо указать название игры']);
		// Проверка типов игроков
		if($scope.newGameInfo.sides.white.type==0 || $scope.newGameInfo.sides.white.type==2)
			return WCApi.riseError('',['Неуказан тип игрока или выбран неактивный тип']);
		if($scope.newGameInfo.sides.blue.type==0 || $scope.newGameInfo.sides.blue.type==2)
			return WCApi.riseError('',['Неуказан тип игрока или выбран неактивный тип']);
		if($scope.newGameInfo.sides.black.type==0 || $scope.newGameInfo.sides.black.type==2)
			return WCApi.riseError('',['Неуказан тип игрока или выбран неактивный тип']);
		if($scope.newGameInfo.sides.red.type==0 || $scope.newGameInfo.sides.red.type==2 )
			return WCApi.riseError('',['Неуказан тип игрока или выбран неактивный тип']);
        if($scope.newGameInfo.sides.red.type==0 || $scope.newGameInfo.sides.red.type==2 )
			return WCApi.riseError('',['Неуказан тип игрока или выбран неактивный тип']);
		// Проверка пароля
		if($scope.newGameInfo.require_password==true && $scope.newGameInfo.password== "")
			return WCApi.riseError('',['Укажите пароль']);
		if(typeof $scope.newGameInfo.time_limit != "number")
			return WCApi.riseError('',['Укажите только число минут']);
	
        // Делаем запрос на создание партии в serverside
        WCApi.send("newGame",$scope.newGameInfo)
		.then(function(data){
		});
	}

    // Регистрация
    $scope.register = function(){
	    if(typeof ($scope.registerForm.email) == 'undefined' || typeof
            ($scope.registerForm.email) == 'undefined'){
			return WCApi.riseError('',['Заполните пустые поля']);
			console.log($scope.registerForm);
		}
		else{
			WCApi.send("register", $scope.registerForm).then(function(data){
				
			});
		}
	
    }

    // Подтверждение регистрации
    $scope.checkRegister = function(){
	    if(typeof ($scope.checkRegister== 'undefined')){
			return WCApi.riseError('',['Заполните пустые поля']);
			console.log($scope.checkRegister);
		}
		else{
			WCApi.send("checkRegister", $scope.registerForm).then(function(data){
				
			});
		}
    }

    //Восстановление пароля
    $scope.remindPassword = function(){
        console.log($scope.remindPasswordForm);
	    if(typeof($scope.remindPasswordForm.email) == 'undefined' || $scope.remindPasswordForm.email == ''){
			return WCApi.riseError('',['Заполните email']);
		}
        WCApi.send("remindPassword", $scope.registerForm)
        .then(function(data){
        });
    }
});


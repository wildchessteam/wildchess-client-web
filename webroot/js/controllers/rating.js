// Контроллер для обработки TOP-рейтинга
main.controller('topRating',function($scope,$http,$location,$cookies,$routeParams){
    // Создаём объект для работы с сервером
    var WCApi = new ServerSide($http,$cookies);
    
    // Получаем с сервера и обрабатываем список начислений/списаний gjkmpjdfntkz
    WCApi.send('topRating',{})
    .then(function(data){ 
        // Передаём полученные с сервера данные в шаблон
        $scope.topRating = data;
        $scope.$apply();
    });
    
});


// Контроллер для обработки рейтинга пользователя
main.controller('userRating',function($scope,$http,$location,$cookies,$routeParams){
    // Создаём объект для работы с сервером
    var WCApi = new ServerSide($http,$cookies);
    
    // Получаем id пользователя
    let userId = $routeParams.userId;
    
    // Получаем с сервера и обрабатываем список начислений/списаний gjkmpjdfntkz
    WCApi.send('userRating',{id:userId})
    .then(function(data){ 
        // Передаём полученные с сервера данные в шаблон
        for(i in data.history){
            data.history[i]['date'] = WCApi.localDatetime(data.history[i].timestamp);
			data.history[i]['debit'] = (parseInt(data.history[i].rating)>0?true:false);
        }
        console.log(data);
        
        $scope.userRating = data;
        $scope.$apply();
    });
});



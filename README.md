wildchess-client(web)
==============

##  Локальная установка проекта

- Создать каталог проекта
- Клонировать в каталог проекта этот репозиторий
- Установить расширение chrome [Web Server for Chrome](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb)
- Запустить приложение Web Server for Chrome со следующими настройками
	+ CHOOSE FOLDER - выбрать папку wildchess-client-web/webroot из каталога проекта
	+ Web-server - started
	+ Option
		* Run in background
		* Automatically show index.html
		* Enter port - 8887
- Открыть в браузере адрес [http://127.0.0.1:8887]()

Теперь изменения, вносимые в файлы папки wildchess-client-web/webroot можно обозревать через браузер




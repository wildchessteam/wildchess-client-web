#!/bin/bash

source .env
cd build
echo $CI_PASSWORD|docker login -u $CI_USERNAME --password-stdin  $CI_REGISTRY
docker-compose build
docker-compose push
